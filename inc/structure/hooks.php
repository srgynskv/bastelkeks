<?php
/**
 * bastelkeks hooks
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

/**
 * General
 * @see  bastelkeks_setup()
 * @see  bastelkeks_widgets_init()
 * @see  bastelkeks_scripts()
 * @see  bastelkeks_header_widget_region()
 * @see  bastelkeks_get_sidebar()
 */
add_action( 'after_setup_theme',			'bastelkeks_setup' );
add_action( 'widgets_init',					'bastelkeks_widgets_init' );
add_action( 'wp_enqueue_scripts',			'bastelkeks_scripts',				10 );
// add_action( 'bastelkeks_before_content',	'bastelkeks_header_widget_region',	10 );
add_action( 'bastelkeks_sidebar',			'bastelkeks_get_sidebar',			10 );

/**
 * Header
 * @see  bastelkeks_secondary_navigation()
 * @see  bastelkeks_site_branding()
 * @see  bastelkeks_primary_navigation()
 */
add_action( 'bastelkeks_header', 'bastelkeks_secondary_navigation',	20 );
add_action( 'bastelkeks_header', 'bastelkeks_site_branding',		30 );
add_action( 'bastelkeks_header', 'bastelkeks_primary_navigation',	60 );

/**
 * Footer
 * @see  bastelkeks_footer_widgets()
 * @see  bastelkeks_credit()
 */
add_action( 'bastelkeks_footer', 'bastelkeks_footer_widgets',	10 );
// add_action( 'bastelkeks_footer', 'bastelkeks_credit',			20 );

/**
 * Homepage
 * @see  bastelkeks_homepage_content()
 * @see  bastelkeks_product_categories()
 * @see  bastelkeks_recent_products()
 * @see  bastelkeks_featured_products()
 * @see  bastelkeks_popular_products()
 * @see  bastelkeks_on_sale_products()
 * @see  bastelkeks_homepage_news()
 * @see  bastelkeks_homepage_slider()
 */
add_action( 'homepage', 'bastelkeks_homepage_content',		10 );
add_action( 'homepage', 'bastelkeks_product_categories',	20 );
add_action( 'homepage', 'bastelkeks_recent_products',		30 );
add_action( 'homepage', 'bastelkeks_featured_products',		40 );
add_action( 'homepage', 'bastelkeks_popular_products',		50 );
add_action( 'homepage', 'bastelkeks_on_sale_products',		60 );
add_action( 'homepage', 'bastelkeks_homepage_news',			70 );
add_action( 'homepage', 'bastelkeks_homepage_slider',		80 );

/**
 * Posts
 * @see  bastelkeks_post_header()
 * @see  bastelkeks_post_meta()
 * @see  bastelkeks_post_content()
 * @see  bastelkeks_paging_nav()
 * @see  bastelkeks_single_post_header()
 * @see  bastelkeks_post_nav()
 * @see  bastelkeks_display_comments()
 */
add_action( 'bastelkeks_loop_post',			'bastelkeks_post_header',		10 );
add_action( 'bastelkeks_loop_post',			'bastelkeks_post_meta',			20 );
add_action( 'bastelkeks_loop_post',			'bastelkeks_post_content',		30 );
add_action( 'bastelkeks_loop_after',		'bastelkeks_paging_nav',		10 );
add_action( 'bastelkeks_single_post',		'bastelkeks_post_header',		10 );
add_action( 'bastelkeks_single_post',		'bastelkeks_post_meta',			20 );
add_action( 'bastelkeks_single_post',		'bastelkeks_post_content',		30 );
add_action( 'bastelkeks_single_post_after',	'bastelkeks_post_nav',			10 );
add_action( 'bastelkeks_single_post_after',	'bastelkeks_display_comments',	10 );

/**
 * Pages
 * @see  bastelkeks_page_header()
 * @see  bastelkeks_page_content()
 * @see  bastelkeks_display_comments()
 */
add_action( 'bastelkeks_page', 			'bastelkeks_page_header',		10 );
add_action( 'bastelkeks_page', 			'bastelkeks_page_content',		20 );
add_action( 'bastelkeks_page_after', 	'bastelkeks_display_comments',	10 );

/**
 * Extras
 * @see  bastelkeks_setup_author()
 * @see  bastelkeks_body_classes()
 * @see  bastelkeks_page_menu_args()
 */
add_filter( 'body_class',			'bastelkeks_body_classes' );
add_filter( 'wp_page_menu_args',	'bastelkeks_page_menu_args' );
