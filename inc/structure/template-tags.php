<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( ! function_exists( 'bastelkeks_product_categories' ) ) {
	/**
	 * Display Product Categories
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function bastelkeks_product_categories( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'bastelkeks_product_categories_args', array(
				'limit' 			=> 3,
				'columns' 			=> 3,
				'child_categories' 	=> 0,
				'orderby' 			=> 'name',
				'title'				=> __( 'Product Categories', 'bastelkeks' ),
				) );

			echo '<section class="bastelkeks-product-section bastelkeks-product-categories">';

			echo '<h2 class="section-title">' . esc_attr( $args['title'] ) . '</h2>';
			echo do_shortcode( '[product_categories number="' . $args['limit'] . '" columns="' . $args['columns'] . '" orderby="' . $args['orderby'] . '" parent="' . $args['child_categories'] . '"]' );

			echo '</section>';

		}
	}
}

if ( ! function_exists( 'bastelkeks_recent_products' ) ) {
	/**
	 * Display Recent Products
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function bastelkeks_recent_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'bastelkeks_recent_products_args', array(
				'limit'		=> 4,
				'columns'	=> 4,
				'title'		=> __( 'Recent Products', 'bastelkeks' ),
			) );

			echo '<section class="bastelkeks-product-section bastelkeks-recent-products">';
			// echo '<h2 class="section-title">' . esc_attr( $args['title'] ) . '</h2>';
			echo '<img src="' . get_stylesheet_directory_uri() . '/images/shop.png' . '" alt="' . __( 'Bastelkeks recent products', 'bastelkeks' ) . '">';
			echo do_shortcode( '[recent_products per_page="' . intval( $args['limit'] ) . '" columns="' . intval( $args['columns'] ) . '"]' );
			echo '</section>';

		}
	}
}

if ( ! function_exists( 'bastelkeks_featured_products' ) ) {
	/**
	 * Display Featured Products
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function bastelkeks_featured_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'bastelkeks_featured_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'title'				=> __( 'Featured Products', 'bastelkeks' ),
				) );

			echo '<section class="bastelkeks-product-section bastelkeks-feautred-products">';

			echo '<h2 class="section-title">' . esc_attr( $args['title'] ) . '</h2>';
			echo do_shortcode( '[featured_products per_page="' . intval( $args['limit'] ) . '" columns="' . intval( $args['columns'] ) . '"]' );

			echo '</section>';

		}
	}
}

if ( ! function_exists( 'bastelkeks_popular_products' ) ) {
	/**
	 * Display Popular Products
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function bastelkeks_popular_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'bastelkeks_popular_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'title'				=> __( 'Top Rated Products', 'bastelkeks' ),
				) );

			echo '<section class="bastelkeks-product-section bastelkeks-popular-products">';

			echo '<h2 class="section-title">' . esc_attr( $args['title'] ) . '</h2>';
			echo do_shortcode( '[top_rated_products per_page="' . intval( $args['limit'] ) . '" columns="' . intval( $args['columns'] ) . '"]' );

			echo '</section>';

		}
	}
}

if ( ! function_exists( 'bastelkeks_on_sale_products' ) ) {
	/**
	 * Display On Sale Products
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function bastelkeks_on_sale_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'bastelkeks_on_sale_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'title'				=> __( 'On Sale', 'bastelkeks' ),
				) );

			echo '<section class="bastelkeks-product-section bastelkeks-on-sale-products">';

			echo '<h2 class="section-title">' . esc_attr( $args['title'] ) . '</h2>';
			echo do_shortcode( '[sale_products per_page="' . intval( $args['limit'] ) . '" columns="' . intval( $args['columns'] ) . '"]' );

			echo '</section>';

		}
	}
}

if ( ! function_exists( 'bastelkeks_homepage_content' ) ) {
	/**
	 * Display homepage content
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return  void
	 */
	function bastelkeks_homepage_content() {

		while ( have_posts() ) : the_post();

			remove_filter( 'bastelkeks_page', 'bastelkeks_page_header', 10 ); ?>

			<div class="homepage-content">
				<?php get_template_part( 'content', 'page' ); ?>
			</div>

		<?php endwhile;

	}
}

if ( ! function_exists( 'bastelkeks_social_icons' ) ) {
	/**
	 * Display social icons
	 * If the subscribe and connect plugin is active, display the icons.
	 * @link http://wordpress.org/plugins/subscribe-and-connect/
	 * @since 1.0.0
	 */
	function bastelkeks_social_icons() {

		if ( class_exists( 'Subscribe_And_Connect' ) ) {

			echo '<div class="subscribe-and-connect-connect">';
			subscribe_and_connect_connect();
			echo '</div>';

		}

	}
}

if ( ! function_exists( 'bastelkeks_get_sidebar' ) ) {
	/**
	 * Display bastelkeks sidebar
	 * @uses get_sidebar()
	 * @since 1.0.0
	 */
	function bastelkeks_get_sidebar() {
		get_sidebar();
	}
}

if ( ! function_exists( 'bastelkeks_homepage_news' ) ) {
	/**
	 * Displays homepage news teaser.
	 *
	 * @since 1.4.0
	 * @return void
	 */
	function bastelkeks_homepage_news() {

		$teaser = get_page_by_path( 'news' );

		if ( ! empty( $teaser ) ) { ?>

		<div class="homepage-news">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/news.png" alt="<?php _e( 'Bastelkeks latest news' ); ?>">
			<div class="homepage-news-text">
				<div class="homepage-news-text-content">
					<?php echo wpautop( esc_html( $teaser->post_content ) ); ?>
				</div>
			</div>
		</div>
		<div class="clear"></div>

		<?php }

	}
}

if ( ! function_exists( 'bastelkeks_homepage_slider' ) ) {
	/**
	 * Displays homepage slider
	 * @since 1.4.0
	 * @return void
	 */
	function bastelkeks_homepage_slider() {

		if ( is_active_sidebar( 'slider-1' ) ) { ?>

		<div class="homepage-slider">
			<div class="col-full">
				<?php dynamic_sidebar( 'slider-1' ); ?>
			</div>
		</div>
		<div class="clear"></div>

		<?php }

	}
}
