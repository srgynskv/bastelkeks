<?php
/**
 * bastelkeks customizer hooks
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

add_action( 'customize_preview_init', 	'bastelkeks_customize_preview_js' );
add_action( 'customize_register', 		'bastelkeks_customize_register' );
add_filter( 'body_class', 				'bastelkeks_layout_class' );
add_action( 'wp_enqueue_scripts', 		'bastelkeks_add_customizer_css', 130 );
add_action( 'after_setup_theme', 		'bastelkeks_custom_header_setup' );
