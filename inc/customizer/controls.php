<?php
/**
 * bastelkeks Theme Customizer controls
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

/**
 * Add postMessage support for site title and description for the Theme Customizer along with several other settings.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 * @since  1.0.0
 */
if ( ! function_exists( 'bastelkeks_customize_register' ) ) {
	function bastelkeks_customize_register( $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

		// Move background color setting alongside background image
		$wp_customize->get_control( 'background_color' )->section 	= 'background_image';
		$wp_customize->get_control( 'background_color' )->priority 	= 20;

		// Change background image section title & priority
		$wp_customize->get_section( 'background_image' )->title 	= __( 'Background', 'bastelkeks' );
		$wp_customize->get_section( 'background_image' )->priority 	= 30;

		// Change header image section title & priority
		$wp_customize->get_section( 'header_image' )->title 		= __( 'Header', 'bastelkeks' );
		$wp_customize->get_section( 'header_image' )->priority 		= 35;

		/**
		 * Custom controls
		 */
		require_once dirname( __FILE__ ) . '/controls/layout.php';
		require_once dirname( __FILE__ ) . '/controls/arbitrary.php';

		/**
		 * Add the typography section
	     */
		$wp_customize->add_section( 'bastelkeks_typography' , array(
			'title'      => __( 'Typography', 'bastelkeks' ),
			'priority'   => 45,
		) );

		/**
		 * Accent Color
		 */
		$wp_customize->add_setting( 'bastelkeks_accent_color', array(
			'default'           => apply_filters( 'bastelkeks_default_accent_color', '#96588a' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_accent_color', array(
			'label'	   => 'Link / accent color',
			'section'  => 'bastelkeks_typography',
			'settings' => 'bastelkeks_accent_color',
			'priority' => 20,
		) ) );

		/**
		 * Text Color
		 */
		$wp_customize->add_setting( 'bastelkeks_text_color', array(
			'default'           => apply_filters( 'bastelkeks_default_text_color', '#6c717a' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport'			=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_text_color', array(
			'label'		=> 'Text color',
			'section'	=> 'bastelkeks_typography',
			'settings'	=> 'bastelkeks_text_color',
			'priority'	=> 30,
		) ) );

		/**
		 * Heading color
		 */
		$wp_customize->add_setting( 'bastelkeks_heading_color', array(
			'default'           => apply_filters( 'bastelkeks_default_heading_color', '#484c51' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport'			=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_heading_color', array(
			'label'	   => 'Heading color',
			'section'  => 'bastelkeks_typography',
			'settings' => 'bastelkeks_heading_color',
			'priority' => 40,
		) ) );

		/**
		 * Header Background
		 */
		$wp_customize->add_setting( 'bastelkeks_header_background_color', array(
			'default'           => apply_filters( 'bastelkeks_default_header_background_color', '#2c2d33' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport'			=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_header_background_color', array(
			'label'	   => 'Background color',
			'section'  => 'header_image',
			'settings' => 'bastelkeks_header_background_color',
			'priority' => 15,
		) ) );

		/**
		 * Header text color
		 */
		$wp_customize->add_setting( 'bastelkeks_header_text_color', array(
			'default'           => apply_filters( 'bastelkeks_default_header_text_color', '#9aa0a7' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport'			=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_header_text_color', array(
			'label'	   => 'Text color',
			'section'  => 'header_image',
			'settings' => 'bastelkeks_header_text_color',
			'priority' => 20,
		) ) );

		/**
		 * Header link color
		 */
		$wp_customize->add_setting( 'bastelkeks_header_link_color', array(
			'default'           => apply_filters( 'bastelkeks_default_header_link_color', '#ffffff' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport'			=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_header_link_color', array(
			'label'	   => 'Link color',
			'section'  => 'header_image',
			'settings' => 'bastelkeks_header_link_color',
			'priority' => 30,
		) ) );

		/**
		 * Footer section
		 */
		$wp_customize->add_section( 'bastelkeks_footer' , array(
			'title'      	=> __( 'Footer', 'bastelkeks' ),
			'priority'   	=> 40,
			'description' 	=> __( 'Customise the look & feel of your web site footer.', 'bastelkeks' ),
		) );

		/**
		 * Footer heading color
		 */
		$wp_customize->add_setting( 'bastelkeks_footer_heading_color', array(
			'default'           => apply_filters( 'bastelkeks_default_footer_heading_color', '#494c50' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport' 		=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_footer_heading_color', array(
			'label'	   	=> 'Heading color',
			'section'  	=> 'bastelkeks_footer',
			'settings' 	=> 'bastelkeks_footer_heading_color',
		) ) );

		/**
		 * Footer text color
		 */
		$wp_customize->add_setting( 'bastelkeks_footer_text_color', array(
			'default'           => apply_filters( 'bastelkeks_default_footer_text_color', '#61656b' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport'			=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_footer_text_color', array(
			'label'	   => 'Text color',
			'section'  => 'bastelkeks_footer',
			'settings' => 'bastelkeks_footer_text_color',
		) ) );

		/**
		 * Footer link color
		 */
		$wp_customize->add_setting( 'bastelkeks_footer_link_color', array(
			'default'           => apply_filters( 'bastelkeks_default_footer_link_color', '#96588a' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport'			=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_footer_link_color', array(
			'label'	   => 'Link color',
			'section'  => 'bastelkeks_footer',
			'settings' => 'bastelkeks_footer_link_color',
		) ) );

		/**
		 * Footer Background
		 */
		$wp_customize->add_setting( 'bastelkeks_footer_background_color', array(
			'default'           => apply_filters( 'bastelkeks_default_footer_background_color', '#f3f3f3' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
			'transport'			=> 'postMessage',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_footer_background_color', array(
			'label'	   => 'Background color',
			'section'  => 'bastelkeks_footer',
			'settings' => 'bastelkeks_footer_background_color',
		) ) );

		/**
		 * Buttons section
		 */
		$wp_customize->add_section( 'bastelkeks_buttons' , array(
			'title'      	=> __( 'Buttons', 'bastelkeks' ),
			'priority'   	=> 45,
			'description' 	=> __( 'Customise the look & feel of your web site buttons.', 'bastelkeks' ),
		) );

		/**
		 * Button background color
		 */
		$wp_customize->add_setting( 'bastelkeks_button_background_color', array(
			'default'           => apply_filters( 'bastelkeks_default_button_background_color', '#6c717a' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_button_background_color', array(
			'label'	   => 'Background color',
			'section'  => 'bastelkeks_buttons',
			'settings' => 'bastelkeks_button_background_color',
			'priority' => 10,
		) ) );

		/**
		 * Button text color
		 */
		$wp_customize->add_setting( 'bastelkeks_button_text_color', array(
			'default'           => apply_filters( 'bastelkeks_default_button_text_color', '#ffffff' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_button_text_color', array(
			'label'	   => 'Text color',
			'section'  => 'bastelkeks_buttons',
			'settings' => 'bastelkeks_button_text_color',
			'priority' => 20,
		) ) );

		/**
		 * Button alt background color
		 */
		$wp_customize->add_setting( 'bastelkeks_button_alt_background_color', array(
			'default'           => apply_filters( 'bastelkeks_default_button_alt_background_color', '#96588a' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_button_alt_background_color', array(
			'label'	   => 'Alternate button background color',
			'section'  => 'bastelkeks_buttons',
			'settings' => 'bastelkeks_button_alt_background_color',
			'priority' => 30,
		) ) );

		/**
		 * Button alt text color
		 */
		$wp_customize->add_setting( 'bastelkeks_button_alt_text_color', array(
			'default'           => apply_filters( 'bastelkeks_default_button_alt_text_color', '#ffffff' ),
			'sanitize_callback' => 'bastelkeks_sanitize_hex_color',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bastelkeks_button_alt_text_color', array(
			'label'	   => 'Alternate button text color',
			'section'  => 'bastelkeks_buttons',
			'settings' => 'bastelkeks_button_alt_text_color',
			'priority' => 40,
		) ) );

		/**
		 * Layout
		 */
		$wp_customize->add_section( 'bastelkeks_layout' , array(
			'title'      	=> __( 'Layout', 'bastelkeks' ),
			'priority'   	=> 50,
		) );

		$wp_customize->add_setting( 'bastelkeks_layout', array(
			'default'    		=> 'right',
			'sanitize_callback' => 'bastelkeks_sanitize_layout',
		) );

		$wp_customize->add_control( new Layout_Picker_Bastelkeks_Control( $wp_customize, 'bastelkeks_layout', array(
			'label'    => __( 'General layout', 'bastelkeks' ),
			'section'  => 'bastelkeks_layout',
			'settings' => 'bastelkeks_layout',
			'priority' => 1,
		) ) );

		$wp_customize->add_control( new Arbitrary_Bastelkeks_Control( $wp_customize, 'bastelkeks_divider', array(
			'section'  	=> 'bastelkeks_layout',
			'type' 		=> 'divider',
			'priority' 	=> 2,
		) ) );

		/**
		 * More
		 */
		if ( apply_filters( 'bastelkeks_customizer_more', true ) ) {
			$wp_customize->add_section( 'bastelkeks_more' , array(
				'title'      	=> __( 'More', 'bastelkeks' ),
				'priority'   	=> 999,
			) );

			$wp_customize->add_setting( 'bastelkeks_more', array(
				'default'    		=> null,
				'sanitize_callback' => 'sanitize_text_field',
			) );

		}
	}
}
