<?php
/**
 * bastelkeks WooCommerce hooks
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

/**
 * Styles
 * @see  bastelkeks_woocommerce_scripts()
 */
add_action( 'wp_enqueue_scripts',			'bastelkeks_woocommerce_scripts', 20 );
add_filter( 'woocommerce_enqueue_styles',	'__return_empty_array' );

/**
 * Layout
 * @see  bastelkeks_before_content()
 * @see  bastelkeks_after_content()
 * @see  woocommerce_breadcrumb()
 */
remove_action( 'woocommerce_before_main_content', 	'woocommerce_breadcrumb', 					20, 0 );
remove_action( 'woocommerce_before_main_content', 	'woocommerce_output_content_wrapper', 		10 );
remove_action( 'woocommerce_after_main_content', 	'woocommerce_output_content_wrapper_end', 	10 );
remove_action( 'woocommerce_sidebar', 				'woocommerce_get_sidebar', 					10 );
add_action( 'woocommerce_before_main_content', 		'bastelkeks_before_content', 				10 );
add_action( 'woocommerce_after_main_content', 		'bastelkeks_after_content', 				10 );
add_action( 'bastelkeks_content_top', 				'woocommerce_breadcrumb', 					10 );

/**
 * Products
 * @see  bastelkeks_upsell_display()
 */
remove_action( 'woocommerce_after_single_product_summary', 	'woocommerce_upsell_display', 				15 );
add_action( 'woocommerce_after_single_product_summary', 	'bastelkeks_upsell_display', 				15 );
remove_action( 'woocommerce_before_shop_loop_item_title', 	'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 		'woocommerce_show_product_loop_sale_flash', 6 );


/**
 * Header
 * @see  bastelkeks_product_search()
 * @see  bastelkeks_header_cart()
 */
// add_action( 'bastelkeks_header', 'bastelkeks_product_search',	40 );
add_action( 'bastelkeks_header', 'bastelkeks_header_cart',		50 );

/**
 * Filters
 * @see  bastelkeks_woocommerce_body_class()
 * @see  bastelkeks_cart_link_fragment()
 * @see  bastelkeks_thumbnail_columns()
 * @see  bastelkeks_related_products_args()
 * @see  bastelkeks_products_per_page()
 * @see  bastelkeks_loop_columns()
 * @see  bastelkeks_breadcrumb_delimeter()
 */
add_filter( 'body_class', 								'bastelkeks_woocommerce_body_class' );
add_filter( 'woocommerce_product_thumbnails_columns', 	'bastelkeks_thumbnail_columns' );
add_filter( 'woocommerce_output_related_products_args', 'bastelkeks_related_products_args' );
add_filter( 'loop_shop_per_page', 						'bastelkeks_products_per_page' );
add_filter( 'loop_shop_columns', 						'bastelkeks_loop_columns' );

if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.3', '>=' ) ) {
	add_filter( 'woocommerce_add_to_cart_fragments', 'bastelkeks_cart_link_fragment' );
} else {
	add_filter( 'add_to_cart_fragments', 'bastelkeks_cart_link_fragment' );
}

/**
 * Integrations
 * @see  bastelkeks_woocommerce_integrations_scripts()
 * @see  bastelkeks_add_bookings_customizer_css()
 */
add_action( 'wp_enqueue_scripts', 'bastelkeks_woocommerce_integrations_scripts' );
add_action( 'wp_enqueue_scripts', 'bastelkeks_add_integrations_customizer_css' );
