<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function bastelkeks_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'bastelkeks_jetpack_setup' );
