<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php bastelkeks_html_tag_schema(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
	wp_head();
	get_template_part( 'favicons' );
?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#site-navigation"><?php _e( 'Skip to navigation', 'bastelkeks' ); ?></a>
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'bastelkeks' ); ?></a>

	<?php do_action( 'bastelkeks_before_header' ); ?>

	<header id="masthead" class="site-header" role="banner" <?php if ( get_header_image() != '' ) { echo 'style="background-image: url(' . esc_url( get_header_image() ) . ');"'; } ?>>
		<div class="col-full">

			<?php
			/**
			 * @hooked bastelkeks_social_icons - 10
			 * @hooked bastelkeks_secondary_navigation - 20
			 * @hooked bastelkeks_site_branding - 30
			 * // @hooked bastelkeks_product_search - 40
			 * @hooked bastelkeks_header_cart - 50
			 * @hooked bastelkeks_primary_navigation - 60
			 */
			do_action( 'bastelkeks_header' ); ?>

		</div>
	</header><!-- #masthead -->

	<?php
	/**
	 * @hooked bastelkeks_header_widget_region - 10
	 */
	do_action( 'bastelkeks_before_content' ); ?>

	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		<?php
		/**
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action( 'bastelkeks_content_top' );
