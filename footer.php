<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'bastelkeks_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="content-info">
		<div class="col-full">

			<?php
			/**
			 * @hooked bastelkeks_footer_widgets - 10
			 * // @hooked bastelkeks_credit - 20
			 */
			do_action( 'bastelkeks_footer' ); ?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'bastelkeks_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
