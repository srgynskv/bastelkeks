/* jshint node:true */
module.exports = function( grunt ) {
	'use strict';

	grunt.initConfig({

		// JavaScript linting with JSHint.
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			all: [
				'Gruntfile.js',
				'js/*.js',
				'!js/*.min.js',
				'inc/customizer/js/*.js',
				'!inc/customizer/js/*.min.js'
			]
		},

		// Minify .js files.
		uglify: {
			options: {
				preserveComments: 'some'
			},
			main: {
				files: [{
					expand: true,
					cwd: 'js/',
					src: [
						'*.js',
						'!*.min.js'
					],
					dest: 'js/',
					ext: '.min.js'
				}]
			},
			customizer: {
				files: [{
					expand: true,
					cwd: 'inc/customizer/js/',
					src: [
						'*.js',
						'!*.min.js'
					],
					dest: 'inc/customizer/js/',
					ext: '.min.js'
				}]
			}
		},

		// Compile all .scss files.
		sass: {
			dist: {
				options: {
					require: 'susy',
					sourcemap: 'none',
					includePaths: require( 'node-bourbon' ).includePaths,
					outputStyle: 'compressed'
				},
				files: [{
					'style.css': 'style.scss',
					'inc/woocommerce/css/bookings.css': 'inc/woocommerce/css/bookings.scss',
					'inc/woocommerce/css/brands.css': 'inc/woocommerce/css/brands.scss',
					'inc/woocommerce/css/wishlists.css': 'inc/woocommerce/css/wishlists.scss',
					'inc/woocommerce/css/ajax-layered-nav.css': 'inc/woocommerce/css/ajax-layered-nav.scss',
					'inc/woocommerce/css/variation-swatches.css': 'inc/woocommerce/css/variation-swatches.scss',
					'inc/woocommerce/css/composite-products.css': 'inc/woocommerce/css/composite-products.scss',
					'inc/woocommerce/css/photography.css': 'inc/woocommerce/css/photography.scss',
					'inc/woocommerce/css/product-reviews-pro.css': 'inc/woocommerce/css/product-reviews-pro.scss',
					'inc/woocommerce/css/smart-coupons.css': 'inc/woocommerce/css/smart-coupons.scss',
					'inc/woocommerce/css/woocommerce.css': 'inc/woocommerce/css/woocommerce.scss'
				}]
			}
		},

		// Minify all .css files.
		cssmin: {
			minify: {
				expand: true,
				cwd: 'inc/woocommerce/css/',
				src: ['*.css'],
				dest: 'inc/woocommerce/css/',
				ext: '.css'
			}
		},

		// Watch changes for assets.
		watch: {
			css: {
				files: [
					'style.scss',
					'inc/woocommerce/css/*.scss',
					'sass/base/*.scss',
					'sass/components/*.scss',
					'sass/font-awesome/*.scss',
					'sass/layout/*.scss',
					'sass/utils/*.scss',
					'sass/vendors/*.scss'
				],
				tasks: [
					'sass',
					'css'
				]
			},
			js: {
				files: [
					// main js
					'js/*js',
					'!js/*.min.js',

					// customizer js
					'inc/customizer/js/*js',
					'!inc/customizer/js/*.min.js'
				],
				tasks: ['uglify']
			}
		},

		// POT
		makepot: {
			target: {
				options: {
					domainPath: 'languages',
					potFilename: 'bastelkeks.pot',
					potHeaders: {
						poedit: true,
						'x-poedit-keywordslist': true,
						'report-msgid-bugs-to': '',
						'language-team': 'LANGUAGE <EMAIL@ADDRESS>'
					},
					processPot: function ( pot ) {
						pot.headers['project-id-version'];
						return pot;
					},
					type: 'wp-theme',
					updateTimestamp: true
				}
			}
		},

		// Check textdomain errors.
		checktextdomain: {
			options:{
				text_domain: 'bastelkeks',
				keywords: [
					'__:1,2d',
					'_e:1,2d',
					'_x:1,2c,3d',
					'esc_html__:1,2d',
					'esc_html_e:1,2d',
					'esc_html_x:1,2c,3d',
					'esc_attr__:1,2d',
					'esc_attr_e:1,2d',
					'esc_attr_x:1,2c,3d',
					'_ex:1,2c,3d',
					'_n:1,2,4d',
					'_nx:1,2,4c,5d',
					'_n_noop:1,2,3d',
					'_nx_noop:1,2,3c,4d'
				]
			},
			files: {
				src:  [
					'**/*.php', // Include all files
					'!node_modules/**' // Exclude node_modules/
				],
				expand: true
			}
		},

		// Creates deploy-able theme
		copy: {
			deploy: {
				src: [
					'**',
					'!.*',
					'!.*/**',
					'.htaccess',
					'!.DS_Store',
					'!Gruntfile.js',
					'!npm-debug.log',
					'!package.json',
					// '!style.scss',
					// '!inc/customizer/*.js',
					// 'inc/customizer/*.min.js',
					// '!inc/woocommerce/css/*.scss',
					// '!js/*.js',
					// 'js/*.min.js',
					'!node_modules/**',
					'!sass/**',
				],
				dest: '.bastelkeks/',
				expand: true,
				dot: true
			}
		}

	});

	// Load NPM tasks to be used here
	grunt.loadNpmTasks( 'grunt-contrib-jshint' );
	grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-sass' );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-wp-i18n' );
	grunt.loadNpmTasks( 'grunt-checktextdomain' );
	grunt.loadNpmTasks( 'grunt-contrib-copy' );

	// Register tasks
	grunt.registerTask( 'default', [
		'css',
		'uglify'
	]);

	grunt.registerTask( 'css', [
		'sass',
		'cssmin'
	]);

	grunt.registerTask( 'dev', [
		'default',
		'makepot'
	]);

	grunt.registerTask( 'deploy', [
		'default',
		'makepot',
		'copy'
	]);
};
