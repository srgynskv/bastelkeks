# WARNING! Do not place your translation files here.

Any translation files placed here will be deleted when you update Bastelkeks.

## Translating Bastelkeks
Put any custom Bastelkeks translation files in your WordPress language directory: `/wp-content/languages/themes/bastelkeks-it_IT.mo`.

Alternatively you can put translations in your child theme: `/wp-content/themes/your-child-theme/languages/it_IT.mo`.
