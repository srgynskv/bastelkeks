<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	/**
	 * @hooked bastelkeks_page_header - 10
	 * @hooked bastelkeks_page_content - 20
	 */
	do_action( 'bastelkeks_page' ); ?>

</article><!-- #post-## -->
