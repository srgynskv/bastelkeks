<?php
/**
 * bastelkeks setup functions
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

/**
 * Assign the Bastelkeks version to a var
 */
$theme				= wp_get_theme();
$bastelkeks_version	= $theme['Version'];

if ( ! function_exists( 'bastelkeks_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bastelkeks_setup() {

		/*
		 * Load Localisation files.
		 *
		 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
		 */

		// wp-content/languages/theme-name/it_IT.mo
		load_theme_textdomain( 'bastelkeks', trailingslashit( WP_LANG_DIR ) . 'themes/' );

		// wp-content/themes/child-theme-name/languages/it_IT.mo
		load_theme_textdomain( 'bastelkeks', get_stylesheet_directory() . '/languages' );

		// wp-content/themes/theme-name/languages/it_IT.mo
		load_theme_textdomain( 'bastelkeks', get_template_directory() . '/languages' );

		/**
		 * Add default posts and comments RSS feed links to head.
		 */
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
			'primary'	=> __( 'Primary Menu', 'bastelkeks' ),
			'secondary'	=> __( 'Secondary Menu', 'bastelkeks' ),
			'handheld'	=> __( 'Handheld Menu', 'bastelkeks' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		) );

		// Setup the WordPress core custom background feature.
		// add_theme_support( 'custom-background', apply_filters( 'bastelkeks_custom_background_args', array(
		// 	'default-color' => apply_filters( 'bastelkeks_default_background_color', 'fcfcfc' ),
		// 	'default-image' => '',
		// ) ) );

		// Add support for the Site Logo plugin and the site logo functionality in JetPack
		// https://github.com/automattic/site-logo
		// http://jetpack.me/
		add_theme_support( 'site-logo', array(
			'header-text' => array(
				'site-description',
			),
			'size' => 'full',
		) );

		// Declare WooCommerce support
		add_theme_support( 'woocommerce' );

		// Declare support for title theme feature
		add_theme_support( 'title-tag' );
	}
endif; // bastelkeks_setup

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function bastelkeks_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'bastelkeks' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

	/*
	register_sidebar( array(
		'name'          => __( 'Header', 'bastelkeks' ),
		'id'            => 'header-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	*/

	register_sidebar( array(
		'name'			=> __( 'Slider', 'bastelkeks' ),
		'id'			=> 'slider-1',
		'description'	=> '',
		'before_widget'	=> '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h1 class="widget-title">',
		'after_title'	=> '</h1>',
	) );

	$footer_widget_regions = apply_filters( 'bastelkeks_footer_widget_regions', 4 );

	for ( $i = 1; $i <= intval( $footer_widget_regions ); $i++ ) {
		register_sidebar( array(
			'name' 				=> sprintf( __( 'Footer %d', 'bastelkeks' ), $i ),
			'id' 				=> sprintf( 'footer-%d', $i ),
			'description' 		=> sprintf( __( 'Widgetized Footer Region %d.', 'bastelkeks' ), $i ),
			'before_widget' 	=> '<aside id="%1$s" class="widget %2$s">',
			'after_widget' 		=> '</aside>',
			'before_title' 		=> '<h3>',
			'after_title' 		=> '</h3>',
			)
		);
	}
}

/**
 * Enqueue scripts and styles.
 * @since  1.0.0
 */
function bastelkeks_scripts() {
	global $bastelkeks_version;

	wp_enqueue_style( 'bastelkeks-open-sans', '//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,700,400', array(), $bastelkeks_version );

	wp_enqueue_style( 'bastelkeks-style', get_stylesheet_uri(), array(), $bastelkeks_version );

	wp_enqueue_script( 'bastelkeks-navigation', get_template_directory_uri() . '/js/navigation.min.js', array(), $bastelkeks_version, true );

	wp_enqueue_script( 'bastelkeks-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.min.js', array(), $bastelkeks_version, true );

	wp_enqueue_script( 'bastelkeks-fontsmoothie', get_template_directory_uri() . '/js/fontsmoothie.min.js', array(), $bastelkeks_version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
