<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Full width
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php do_action( 'bastelkeks_page_before' ); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
				/**
				 * @hooked bastelkeks_display_comments - 10
				 */
				do_action( 'bastelkeks_page_after' ); ?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
