<?php
/**
 * The template for displaying all single posts.
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php
			do_action( 'bastelkeks_single_post_before' );

			get_template_part( 'content', 'single' );

			/**
			 * @hooked bastelkeks_post_nav - 10
			 */
			do_action( 'bastelkeks_single_post_after' );
			?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php do_action( 'bastelkeks_sidebar' ); ?>

<?php get_footer();
