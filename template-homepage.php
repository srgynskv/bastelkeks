<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package bastelkeks
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/**
			 * @hooked bastelkeks_homepage_content - 10
			 * @hooked bastelkeks_product_categories - 20
			 * @hooked bastelkeks_recent_products - 30
			 * @hooked bastelkeks_featured_products - 40
			 * @hooked bastelkeks_popular_products - 50
			 * @hooked bastelkeks_on_sale_products - 60
			 */
			do_action( 'homepage' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer();
